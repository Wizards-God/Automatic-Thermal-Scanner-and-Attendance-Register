# Automatic Thermal Scanner and Attendance Register
A Project on Python by the Students of SAMS 8 E
People interested are as follows:
1. Parjanya S
2. Kishan D Belavadi
3. Ashrith K P
4. Suchith R Gowda
5. Varshita V M
6. Namana Raj C
7. Kashish
8. Supriya N
9. Janani V

Things we would need:
1. A WebCam for Facial Recognition
   Open CV for Facial Recognition
2. A Thermal Sensor
   A Program to import the Temperature data into Python
3. An Adruino
   To act as a data storage and processing unit.
